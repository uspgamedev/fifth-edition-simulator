
local equipment_modules = {
  "weapons",
  "armors"
}

local equipment = {}

for _,equipment_group in ipairs(equipment_modules) do
  for name,equip in pairs(require("equipment." .. equipment_group)) do
    equipment[name] = equip
  end
end

return equipment

