
local hit_dice = {}

local sides = { 4, 6, 8, 10, 12, 20 }

for _,dice in ipairs(sides) do
  local name = ("hitdice-d%d"):format(dice)
  hit_dice[name] = function () return HitDice.new(dice) end
end

return hit_dice

