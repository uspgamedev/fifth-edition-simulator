
local effect_modules = {
  "ability_score_change",
  "hit_dice",
  "proficiency"
}

local effects = {}

for _,effect_group in ipairs(effect_modules) do
  for name,effect in pairs(require("effects." .. effect_group)) do
    effects[name] = effect
  end
end

return effects

