
local proficiency = {}

proficiency["proficiency-with-all-weapons"] = function ()
  return WeaponTypeProficiency.new{
    WeaponType.MeleeSimple,
    WeaponType.MeleeMartial,
    WeaponType.RangedSimple,
    WeaponType.RangedMartial
  }
end

return proficiency

