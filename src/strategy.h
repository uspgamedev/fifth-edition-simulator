
#ifndef FES_STRATEGY_H_
#define FES_STRATEGY_H_

#include "combat.h"

#include <tuple>

namespace fes {

class Character;

class Strategy {
  public:
    Strategy() {}
    std::tuple<const Strategy*, Combat::Action*> execute(Character* character, Combat* combat) const;
    static const Strategy* default_strategy();
};

} // namespace fes

#endif // FES_STRATEGY_H_

