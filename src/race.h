
#ifndef FES_RACE_H_
#define FES_RACE_H_

#include "ability.h"
#include "character.h"
#include "effects/effect.h"
#include "effects/ability_score_change.h"

#include <sol2/sol.hpp>

#include <vector>

namespace fes {

class RacialTrait;

class Race {
  public:
    explicit Race(const std::vector<RacialTrait*> &traits);
    auto begin() const { return traits_.begin(); }
    auto end() const { return traits_.end(); }
  private:
    std::vector<RacialTrait*> traits_;
};

class RacialTrait {
  public:
    RacialTrait(const std::vector<const Effect*>& the_effects)
        : effects_(the_effects) {}
    auto begin() const { return effects_.begin(); }
    auto end() const { return effects_.end(); }
  private:
    const std::vector<const Effect*> effects_;
};

} // namespace fes

#endif // FES_RACE_H_

