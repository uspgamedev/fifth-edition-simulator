
#include "team.h"

#include "character.h"

#include <iostream>

namespace fes {

using std::cout;
using std::endl;

void Team::addMember(Character* member) {
    members_.insert(member);
}

bool Team::isAnyoneAlive() const {
    for (Character *member : members_) {
        cout << member->name() << "'s hit points: " << member->hit_points() << endl;
        if (member->conscious())
            return true;
    }
    return false;
}

bool Team::hasMember(Character* character) const {
    return members_.count(character) > 0;
}

} // namespace fes

