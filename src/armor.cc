
#include "armor.h"

using std::string;

namespace fes {

Armor::Armor(const string& the_name, size_t armor_class, Type the_type, Weight the_weight,
               uint64_t the_cost)
    : Equipment(the_name, the_weight, the_cost), armor_class_(armor_class), type_(the_type) {}

} // namespace fes

