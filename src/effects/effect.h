
#ifndef FES_EFFECT_H_
#define FES_EFFECT_H_

#include <memory>
#include <string>
#include <typeinfo>
#include <typeindex>

namespace fes {

class Effect {
  public:
    const static std::string TABLE_NAME;
    virtual ~Effect() {}
    virtual std::type_index type_index() const = 0;
  protected:
    Effect() {}
};

} // namespace fes

#endif // FES_EFFECT_H_


