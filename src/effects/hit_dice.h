
#ifndef FES_HIT_DICE_H_
#define FES_HIT_DICE_H_

#include "character_class.h"
#include "dice.h"

namespace fes {

class HitDice : virtual public Effect {
  public:
    HitDice(const Dice& dice) : hit_dice_(dice) {}
    HitDice(const size_t sides) : HitDice(Dice{1u, sides}) {}
    Dice hit_dice() const { return hit_dice_; }
    void set_original_class(const CharacterClass* the_original_class) {
        original_class_ = the_original_class;
    }
    const CharacterClass* original_class() const { return original_class_; }
    std::type_index type_index() const { return typeid(HitDice); };
  private:
    const CharacterClass *original_class_;
    const Dice hit_dice_;
};

} // namespace fes

#endif // FES_HIT_DICE_H_

