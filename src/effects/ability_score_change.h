
#ifndef FES_ABILITY_SCORE_CHANGE_H_
#define FES_ABILITY_SCORE_CHANGE_H_

#include "ability.h"

#include "effects/effect.h"

#include <tuple>

namespace fes {

class AbilityScoreChange : virtual public Effect {
  public:
    virtual ~AbilityScoreChange() {}
    virtual std::tuple<Ability::Type,int64_t> ability_change() const = 0;
    virtual Ability::Type which_ability() const = 0;
    virtual int64_t change_value() const = 0;
    std::type_index type_index() const { return typeid(AbilityScoreChange); }
  protected:
    AbilityScoreChange() {}
};

class FixedAbilityScoreChange : virtual public AbilityScoreChange {
  public:
    FixedAbilityScoreChange(Ability::Type type, int64_t change)
        : type_(type), change_(change) {}
    std::tuple<Ability::Type,int64_t> ability_change() const override {
        return std::make_tuple(type_, change_);
    }
    Ability::Type which_ability() const { return type_; };
    int64_t change_value() const { return change_; };
  private:
    Ability::Type type_;
    int64_t change_;
};

} // namespace fes

#endif // FES_ABILITY_SCORE_CHANGE_H_

