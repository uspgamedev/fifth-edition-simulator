
#ifndef FES_TEAM_H_
#define FES_TEAM_H_

#include <unordered_set>

namespace fes {

class Character;

class Team {
  public:
    void addMember(Character* member);
    bool isAnyoneAlive() const;
    bool hasMember(Character* character) const;
    Team& operator <<(Character* member) { addMember(member); return *this; }
    auto begin() const { return members_.begin(); }
    auto end() const { return members_.end(); }
    size_t size() const { return members_.size(); }
  private:
    std::unordered_set<Character*> members_;
};

} // namespace fes

#endif // FES_TEAM_H_


