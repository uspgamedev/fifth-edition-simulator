
#include "combat.h"

#include "ability.h"
#include "character.h"
#include "dice.h"
#include "team.h"
#include "weapon.h"

#include <algorithm>
#include <iostream>
#include <utility>

namespace fes {

using std::cout;
using std::endl;
using std::make_pair;
using std::pair;
using std::sort;
using std::transform;
using std::unordered_set;
using std::vector;

void Combat::addTeam(TeamName name, Team* team) {
    name == TeamName::TEAM_ONE ? (team1_ = team) : (team2_ = team);
}

Team* Combat::teamOf(Character* character) const {
    if (team1_->hasMember(character))
        return team1_;
    else if (team2_->hasMember(character))
        return team2_;
    else
        return nullptr;
}

Team* Combat::enemyTeamOf(Character* character) const {
    if (team1_->hasMember(character))
        return team2_;
    else if (team2_->hasMember(character))
        return team1_;
    else
        return nullptr;
}

void Combat::fight() {
    rollInitiatives();
    while (true)
        for (Character *character : order_) {
            cout << "Turn" << endl;
            character->act(this)->execute();
            if (end()) return;
        }
}

void Combat::rollInitiatives() {
    // We must associate an initiative value to each combatant
    vector<pair<Character*,int>> initiatives(team1_->size() + team2_->size(), make_pair(nullptr, 0));
    // Lambda that generates the char-init pair
    auto initiative_maker = [] (Character* character) {
        auto initiative = make_pair(character, character->rollInitiative());
        cout << character->name() << "'s initiative: " << initiative.second << endl;
        return initiative;
    };
    // Generate char-init list
    transform(team1_->begin(), team1_->end(), initiatives.begin(), initiative_maker);
    transform(team2_->begin(), team2_->end(), initiatives.begin() + team1_->size(), initiative_maker);
    // Guess what
    sort(initiatives.begin(), initiatives.end(),
         [](pair<Character*,int> lhs, pair<Character*,int> rhs) {
             return lhs.second > rhs.second;
         });
    // Allocate space before inserting
    order_.resize(initiatives.size(), nullptr);
    // Write to 'order' list
    transform(initiatives.begin(), initiatives.end(), order_.begin(),
              [] (auto initiative) { return initiative.first; });
}

bool Combat::end() const {
    return !team1_->isAnyoneAlive() || !team2_->isAnyoneAlive();
}

void Combat::AttackAction::execute() {
    if (weapon_ != nullptr) {
        if (attacker_->attack(defender_, weapon_))
            cout << attacker_->name() << " hit " << defender_->name() << "!" << endl;
        else
            cout << attacker_->name() << " missed " << defender_->name() << "!" << endl;
    }
}

} // namespace fes

