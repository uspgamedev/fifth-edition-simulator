
#ifndef FES_DAMAGE_H_
#define FES_DAMAGE_H_

#include "dice.h"

#include <iostream>
#include <string>

namespace fes {

constexpr size_t DAMAGE_TYPE_NUM = 13u;

enum class DamageType {
    ACID, BLUDGEONING, COLD, FIRE, FORCE, LIGHTNING, NECROTIC, PIERCING, POISON, PSYCHIC, RADIANT,
    SLASHING, THUNDER
};

const std::string DAMAGE_TYPE_NAMES[DAMAGE_TYPE_NUM] = {
    [static_cast<int>(DamageType::ACID)] = "acid",
    [static_cast<int>(DamageType::BLUDGEONING)] = "bludgeoning",
    [static_cast<int>(DamageType::COLD)] = "cold",
    [static_cast<int>(DamageType::FIRE)] = "fire",
    [static_cast<int>(DamageType::FORCE)] = "force",
    [static_cast<int>(DamageType::LIGHTNING)] = "lightning",
    [static_cast<int>(DamageType::NECROTIC)] = "necrotic",
    [static_cast<int>(DamageType::PIERCING)] = "piercing",
    [static_cast<int>(DamageType::POISON)] = "poison",
    [static_cast<int>(DamageType::PSYCHIC)] = "psychic",
    [static_cast<int>(DamageType::RADIANT)] = "radiant",
    [static_cast<int>(DamageType::SLASHING)] = "slashing",
    [static_cast<int>(DamageType::THUNDER)] = "thunder"
};

struct Damage {
    const size_t amount;
    const DamageType type;
};

struct DamageFormula {
    const Formula formula;
    const DamageType type;
};

inline std::ostream& operator <<(std::ostream &out, const DamageFormula& damage_formula) {
    return out << damage_formula.formula << " ("
               << DAMAGE_TYPE_NAMES[static_cast<int>(damage_formula.type)] << ")" << std::endl;
}

} // namespace fes

#endif // FES_DAMAGE_H_

