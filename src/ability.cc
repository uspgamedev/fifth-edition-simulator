
#include "ability.h"
#include "character.h"
#include "race.h"
#include "effects/ability_score_change.h"

#include <algorithm>
#include <vector>

namespace fes {

using std::vector;
using std::copy_if;

const char* Ability::NAME[NUM] = {
      [static_cast<uint64_t>(Type::STR)] = "STR",
      [static_cast<uint64_t>(Type::DEX)] = "DEX",
      [static_cast<uint64_t>(Type::CON)] = "CON",
      [static_cast<uint64_t>(Type::INT)] = "INT",
      [static_cast<uint64_t>(Type::WIS)] = "WIS",
      [static_cast<uint64_t>(Type::CHA)] = "CHA"
};

Ability::Ability(Character *owner, Type the_type, uint64_t the_value)
    : owner_(owner), type_(the_type), value_(the_value) {}

uint64_t Ability::effective_value() const {
    return std::max(0lu, value_ + all_bonus());
}

int64_t Ability::all_bonus() const {
    int64_t mods = 0l;
    vector<const AbilityScoreChange*> changes =
        owner_->effects<AbilityScoreChange>();
    vector<const AbilityScoreChange*> changes_of_this_ability(changes.size(),
                                                              nullptr);
    auto it = copy_if(changes.begin(), changes.end(), changes_of_this_ability.begin(),
            [this] (const AbilityScoreChange* change) {
                return change->which_ability() == type_;
            });
    changes_of_this_ability.resize(std::distance(changes_of_this_ability.begin(), it));
    for (const AbilityScoreChange *change : changes_of_this_ability) {
        mods += change->change_value();
    }
    return mods;
}

uint64_t Ability::value() const {
    return value_;
}

} // namespace fes

