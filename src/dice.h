
#ifndef FES_DICE_H_
#define FES_DICE_H_

#include <functional>
#include <iostream>
#include <vector>

#include <cstdlib>

namespace fes {

struct Formula;

// Plain Old Data
struct Dice {
    size_t rolls;
    size_t sides;
    size_t roll() const;
    operator Formula() const;
    Dice operator *(size_t rhs) const;
    //Dice& operator =(const Dice& rhs) { rolls = rhs.rolls; sides = rhs.sides; return *this; }
    bool operator ==(const Dice& rhs) const { return rolls == rhs.rolls && sides == rhs.sides; }
};

const Dice d4{1,4};
const Dice d6{1,6};
const Dice d8{1,8};
const Dice d10{1,10};
const Dice d12{1,12};
const Dice d20{1,20};
const Dice d100{1,100};

struct Operand {
    union {
        const Dice dice;
        const size_t num;
    };
    const bool is_dice;
};

using DiceModifier = std::function<size_t (size_t, Dice dice)>;
const DiceModifier IDENTITY = [](size_t value, Dice dice) { return value; };

struct Formula {
    std::vector<Operand> operands;
    size_t eval(DiceModifier dice_modifier = IDENTITY) const;
};

Dice operator *(size_t lhs, const Dice& rhs);
Formula operator +(const Formula &lhs, const Formula &rhs);
Formula operator +(const Formula &lhs, size_t rhs);
Formula operator +(size_t lhs, const Formula &rhs);

inline Dice::operator Formula() const {
    /* Initialization of a Formula needs the initialization of a vector of Operands
     * which, in this case, has only one operand which is initialized with a Dice
     * which is initialized with the 'this' reference */
    return Formula {
        std::vector<Operand> {
            Operand {
                {Dice{*this}}, true
            }
        }
    };
}

inline Dice Dice::operator *(size_t rhs) const {
    return {rhs * rolls, sides};
}

inline Dice operator *(size_t lhs, const Dice& rhs) {
    return rhs * lhs;
}

std::ostream& operator <<(std::ostream& out, const Formula& rhs);

// Dice _2d5 = {2, 5};
// int value = 2*(_2d5 + _4d6 + 10).eval();

} // namespace fes

namespace std {

template<> struct hash<fes::Dice> {
    size_t operator ()(const fes::Dice& dice) const {
        size_t h1 = std::hash<size_t>{}(dice.rolls);
        size_t h2 = std::hash<size_t>{}(dice.sides);
        return h1 ^ (h2 << 1);
    }
};

} // namespace std

#endif // FES_DICE_H_

