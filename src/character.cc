
#include "character.h"

#include "armor.h"
#include "character_class.h"
#include "combat.h"
#include "damage.h"
#include "race.h"
#include "strategy.h"
#include "weapon.h"

#include "effects/hit_dice.h"

#include <algorithm>
#include <iostream>
#include <numeric>
#include <utility>

#include <cmath>

namespace fes {

using std::abs;
using std::accumulate;
using std::endl;
using std::get;
using std::min_element;
using std::min;
using std::ostream;
using std::string;
using std::transform;
using std::tuple;
using std::unordered_set;

namespace {

uint64_t RollAbility() {
    uint64_t values[4];
    uint64_t *begin = &values[0];
    uint64_t *end = &values[4];
    transform(begin, end, begin,
              [] (uint64_t unused) { return d6.roll(); });
    return accumulate(begin, end, 0lu) -
           *min_element(begin, end);
}

} // unnamed namespace

Character::Character(const string &the_name, const Race *the_race, const Strategy* initial_strategy,
                     uint64_t STR, uint64_t DEX, uint64_t CON, uint64_t INT, uint64_t WIS,
                     uint64_t CHA)
    : name_(the_name), hit_point_maximum_(0u), damage_(0u), race_(the_race),
      abilities_{
        Ability(this, Ability::Type::STR, STR),
        Ability(this, Ability::Type::DEX, DEX),
        Ability(this, Ability::Type::CON, CON),
        Ability(this, Ability::Type::INT, INT),
        Ability(this, Ability::Type::WIS, WIS),
        Ability(this, Ability::Type::CHA, CHA),
      }, class_levels_(this), current_strategy_(initial_strategy) {
    for (RacialTrait *trait : *race_)
        for (const Effect *effect : *trait)
            effect_groups_[effect->type_index()].push_back(effect);
}

Character::Character(const string &the_name, const Race *the_race, const Strategy* initial_strategy)
    : Character(the_name, the_race, initial_strategy, RollAbility(), RollAbility(), RollAbility(),
                RollAbility(), RollAbility(), RollAbility()) {}

int64_t Character::modifier(Ability::Type which_type) const {
    uint64_t ability_value =
        abilities_[static_cast<int>(which_type)].effective_value();
    return static_cast<int64_t>(std::floor((ability_value - 10.0)/2.0));
}

Character& Character::operator <<(const CharacterClass* increased_class) {
    size_t level_before = total_level();
    class_levels_.increaseLevel(increased_class);
    for (const HitDice* hit_dice : effects<HitDice>())
        if (hit_dice->original_class() == increased_class) {
            if (level_before >= 1u)
              hit_point_maximum_ += hit_dice->hit_dice().roll();
            else
              hit_point_maximum_ += hit_dice->hit_dice().sides;
        }
    return *this;
}

void Character::equip(Armor *armor) {
    if (armor->is_shield())
        equipped_.insert({"shields", armor});
    else
        equipped_.insert({"armors", armor});
}

void Character::equip(Weapon *weapon) {
    equipped_.insert({"weapons", weapon});
}

void Character::unequip(Armor *armor) {
    string slot = armor->is_shield() ? "shields" : "armors";
    unequipFromSlot(slot, static_cast<Equipment*>(armor));
}

void Character::unequip(Weapon *weapon) {
    unequipFromSlot("weapons", static_cast<Equipment*>(weapon));
}

void Character::unequipFromSlot(const string& equipment_slot, const Equipment *equipment) {
    auto range = equipped_.equal_range(equipment_slot);
    auto to_be_removed = equipped_.end();
    for (auto it = range.first; it != range.second; ++it)
        if (it->second == equipment) {
            to_be_removed = it;
            break;
        }
    if (to_be_removed != equipped_.end())
        equipped_.erase(to_be_removed);
}

const Equipment* Character::findFirstEquipped(const std::string& slot) const {
    auto entry = equipped_.find(slot);
    if (entry != equipped_.end())
        return entry->second;
    else
        return nullptr;
}

const Weapon* Character::findFirstEquippedWeapon() const {
    return dynamic_cast<const Weapon*>(findFirstEquipped("weapons"));
}

size_t Character::armor_class () const {
    using std::min;
    size_t ac = 10u;
    const Armor *donned_armor = dynamic_cast<const Armor*>(findFirstEquipped("armors"));
    if (donned_armor != nullptr) {
        switch(donned_armor->type()) {
          case Armor::Type::Light:
            ac = donned_armor->armor_class() + modifier(Ability::Type::DEX);
            break;
          case Armor::Type::Medium:
            ac = donned_armor->armor_class() + min(2l, modifier(Ability::Type::DEX));
            break;
          case Armor::Type::Heavy:
            ac = donned_armor->armor_class();
            break;
          case Armor::Type::Shield:
          default:
            break;
        }
    }
    const Armor *donned_shield = dynamic_cast<const Armor*>(findFirstEquipped("shields"));
    if (donned_shield != nullptr)
        ac += donned_shield->armor_class();
    return ac;
}

tuple<size_t, Damage> Character::rollAttack(const Weapon *weapon_used) const {
    bool is_critical = false;
    size_t attack_roll = 0u;
    DamageFormula base_damage = weapon_used->damage();
    attack_roll += d20.roll();
    if (attack_roll >= critical_hit_threshold())
        is_critical = true;
    if (has_proficiency(weapon_used))
        attack_roll += proficiency();
    Ability::Type abtype = weapon_used->is_melee() ? Ability::Type::STR : Ability::Type::DEX;
    attack_roll += modifier(abtype);
    DiceModifier mod = is_critical
        ? [](size_t val, Dice dice) { return val + dice.roll(); }
        : IDENTITY;
    Damage damage = Damage{
        (base_damage.formula + modifier(abtype)).eval(mod),
        base_damage.type
    };
    return std::make_tuple(attack_roll, damage);
}

void Character::takeDamage(const Damage& damage) {
    damage_ = min(damage_ + damage.amount, hit_point_maximum());
}

bool Character::attack(Character* target, const Weapon* weapon_used) {
    auto result = rollAttack(weapon_used);
    if (get<0>(result) >= target->armor_class()) {
      target->takeDamage(get<1>(result));
      return true;
    }
    return false;
}

int Character::rollInitiative() const {
    return (d20 + modifier(Ability::Type::DEX)).eval();
}

Combat::Action* Character::act(Combat* combat) {
    tuple<const Strategy*, Combat::Action*> result = current_strategy_->execute(this, combat);
    current_strategy_ = get<0>(result);
    return get<1>(result);
}

void Character::Inventory::insert(const Equipment* new_equipment, size_t amount) {
    items_[new_equipment] += amount;
}

Weight Character::Inventory::total_weight() const {
    return accumulate(items_.begin(), items_.end(), Weight{ 0u, 0u },
                      [] (const Weight& lhs, const auto& rhs) {
                          return lhs + (rhs.first->weight() * rhs.second);
                      });
}

ostream& operator <<(ostream& out, const Character& character) {
    out << character.name() << endl;
    out << "Total level: " << character.total_level() << endl;
    out << "Proficiency bonus: +" << character.proficiency() << endl;
    out << "Hit Dice:";
    for (const Dice& dice : character.hit_dice())
        out << " d" << dice.sides;
    out << endl;
    out << "Hit Points:" << character.hit_points() << "/" << character.hit_point_maximum() << endl;
    out << "AC: " << character.armor_class() << endl;
    out << "Critical Hit Threshold: " << character.critical_hit_threshold() << endl;
    static char buffer[1024];
    for (size_t i = 0; i < Ability::NUM; i++) {
        uint64_t value = character.abilities_[i].value();
        int64_t bonus = character.abilities_[i].effective_value() - value;
        int64_t mod = character.modifier(i);
        sprintf(buffer, "%3s %2lu %c%2ld (%c%2ld)",
                Ability::NAME[i], value, bonus >= 0 ? '+' : '-',
                abs(bonus), mod >= 0 ? '+' : '-', abs(mod));
        out << buffer << endl;
    }
    return out;
}

size_t Character::ClassLevel::levels_in_class(const CharacterClass* which_class) {
    return levels_[which_class];
}

size_t Character::ClassLevel::total_level() const {
    return accumulate(levels_.begin(), levels_.end(), 0u,
                      [] (size_t total, auto &level) {
                          return total + level.second;
                      });
}

void Character::ClassLevel::increaseLevel(const CharacterClass* which_class) {
    size_t level_in_class = ++levels_[which_class];
    for (const ClassFeature* feature : which_class->features_for_level(level_in_class))
        for (const Effect* effect : *feature)
            owner_->effect_groups_[effect->type_index()].push_back(effect);
    for (const HitDice* hit_dice : owner_->effects<HitDice>())
        if (hit_dice->original_class() == which_class)
            hit_dice_.insert(hit_dice->hit_dice());
}

} // namespace fes

