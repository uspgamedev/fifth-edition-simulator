
#ifndef FES_ABILITY_H_
#define FES_ABILITY_H_

#include <cstdlib>
#include <cstdint>

namespace fes {

class Character;

class Ability {
  public:
    enum class Type : uint64_t {
      STR, DEX, CON, INT, WIS, CHA
    };

    static constexpr size_t NUM = 6u;
    static constexpr size_t DEFAULT_VALUE = 10u;

    static const char* NAME[NUM];

    Ability(Character* owner, Type the_type, uint64_t the_value);

    Type type() const { return type_; }

    uint64_t effective_value() const;
    uint64_t value() const;

  private:

    int64_t all_bonus() const;

    Character *owner_;

    Type type_;

    uint64_t value_;
};


} // namespace fes

#endif // FES_ABILITY_H_

