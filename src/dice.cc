
#include "dice.h"

#include <random>
#include <chrono>

namespace fes {

using std::chrono::system_clock;
using std::function;
using std::mt19937;
using std::uniform_int_distribution;

namespace {

mt19937 rng__(system_clock::now().time_since_epoch().count());

}

size_t Dice::roll() const {
    uniform_int_distribution<size_t> distribution(1u, sides);
    size_t value = 0u;
    for (size_t i = 0u; i < rolls; i++)
        value += distribution(rng__);
    return value;
}

size_t Formula::eval(DiceModifier dice_modifier) const {
    size_t value = 0u;
    for (const Operand &op : operands)
        if (op.is_dice)
          value += dice_modifier(op.dice.roll(), op.dice);
        else
          value += op.num;
    return value;
}

Formula operator +(const Formula &lhs, const Formula &rhs) {
    Formula result = lhs;
    for (const Operand &op : rhs.operands)
      result.operands.emplace_back(op);
    return result;
}

Formula operator +(const Formula &lhs, size_t rhs) {
    Formula result = lhs;
    result.operands.emplace_back(Operand{rhs, false});
    return result;
}

Formula operator +(size_t lhs, const Formula &rhs) {
    return rhs + lhs;
}

std::ostream& operator <<(std::ostream& out, const Formula& rhs) {
  bool first = true;
  for (const Operand& op : rhs.operands) {
    if (!first)
      out << " +";
    if (op.is_dice)
      out << op.dice.rolls << "d" << op.dice.sides, first = false;
    else
      out << op.num;
  }
  return out;
}


} // namespace fes

