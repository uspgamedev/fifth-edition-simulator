
#include "strategy.h"

#include "character.h"
#include "combat.h"
#include "team.h"

#include <vector>

namespace fes {

using std::tuple;
using std::make_tuple;
using std::vector;

namespace {
Strategy DEFAULT_STRATEGY;

Combat::AttackAction  attack_action(nullptr, nullptr, nullptr);
Combat::IdleAction    idle_action;
}

tuple<const Strategy*, Combat::Action*> Strategy::execute(Character* character,
                                                         Combat* combat) const {
    Character *target = nullptr;
    Team *enemy_team = combat->enemyTeamOf(character);
    for (Character *possible_target : *enemy_team)
        if (possible_target->conscious()) {
            target = possible_target;
            break;
        }
    if (target != nullptr) {
        attack_action = Combat::AttackAction(character, target,
                                             character->findFirstEquippedWeapon());
        return make_tuple(this, &attack_action);
    } else {
        idle_action = Combat::IdleAction();
        return make_tuple(this, &idle_action);
    }
}

const Strategy* Strategy::default_strategy() {
    return &DEFAULT_STRATEGY;
}

} // namespace fes

