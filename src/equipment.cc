
#include "equipment.h"

#include "armor.h"
#include "config.h"
#include "damage.h"
#include "weapon.h"

#include <functional>
#include <string>

namespace fes {

using std::cout;
using std::endl;
using std::get;
using std::make_tuple;
using std::string;
using std::tuple;

const string Equipment::TABLE_NAME = "equipment";

tuple<uint64_t, uint64_t, uint64_t, uint64_t> Equipment::full_cost() const {
    const uint64_t platinum = platinum_cost();
    const uint64_t gold = (cost_ - 1000*platinum)/100u;
    const uint64_t silver = (cost_ - 1000*platinum - 100*gold)/10u;
    const uint64_t copper = cost_ % 10u;
    return make_tuple(platinum, gold, silver, copper);
}

std::ostream& operator <<(std::ostream &out, const Equipment& equipment) {
    tuple<uint64_t, uint64_t, uint64_t, uint64_t> cost = equipment.full_cost();
    out << "[Equipment: " << equipment.name() << "] { ";
    out << "cost: " << get<0>(cost) << "pp ";
    out             << get<1>(cost) << "gp ";
    out             << get<2>(cost) << "sp ";
    out             << get<3>(cost) << "cp }" << endl;
    return out;
}

} // namespce fes

