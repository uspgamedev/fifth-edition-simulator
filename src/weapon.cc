
#include "weapon.h"

using std::string;

namespace fes {

Weapon::Weapon(const string& the_name, DamageFormula damage, Type the_type, Weight the_weight,
               uint64_t the_cost)
    : Equipment(the_name, the_weight, the_cost), damage_(damage), type_(the_type) {}

uint64_t Weapon::roll() const {
    return damage_.formula.eval();
}

std::ostream& operator <<(std::ostream &out, const Weapon& weapon) {
    out << static_cast<const Equipment&>(weapon);
    out << weapon.damage();
    return out;
}

} // namespace fes

