
#ifndef FES_CHARACTER_CLASS_H_
#define FES_CHARACTER_CLASS_H_

#include "dice.h"

#include "effects/proficiency.h"
#include "utils/iterable_wrapper.h"

#include <sol2/sol.hpp>

#include <string>
#include <vector>

namespace fes {

class ClassFeature {
  public:
    ClassFeature(const std::vector<const Effect*>& the_effects) : effects_(the_effects) {}
    auto begin() const { return effects_.begin(); }
    auto end() const { return effects_.end(); }
    virtual ~ClassFeature() {}
  private:
    const std::vector<const Effect*> effects_;
};

class CharacterClass {
  public:
    using FeatureTable = std::vector<std::vector<const ClassFeature*>>;
    explicit CharacterClass(const FeatureTable &features);
    IterableWrapper<std::vector<const ClassFeature*>> features_for_level(size_t level) const {
        // Levels start from 1 in D&D
        return features_[level-1];
    }
  private:
    const FeatureTable features_;
};

} // namespace fes

#endif // FES_CHARACTER_CLASS_H_

