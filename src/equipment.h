
#ifndef FES_EQUIPMENT_H_
#define FES_EQUIPMENT_H_

#include <iostream>
#include <memory>
#include <string>
#include <tuple>

#include <cstdint>

namespace fes {

struct Weight {
    // In lb
    uint64_t numerator;
    uint64_t denominator;
    Weight operator +(const Weight& rhs) const;
    Weight operator *(uint64_t rhs) const;
};

class Equipment {
  public:
    const static std::string TABLE_NAME;
    Equipment(const std::string the_name, Weight the_weight, uint64_t the_cost)
        : name_(the_name), weight_(the_weight), cost_(the_cost) {}
    virtual ~Equipment() {}
    std::string name() const { return name_; }
    Weight weight() const { return weight_; }
    uint64_t cost() const { return cost_; }
    uint64_t copper_cost() const { return cost_; }
    uint64_t silver_cost() const { return cost_/10u; }
    uint64_t gold_cost() const { return cost_/100u; }
    uint64_t platinum_cost() const { return cost_/1000u; }
    std::tuple<uint64_t, uint64_t, uint64_t, uint64_t> full_cost() const;
    
  private:
    const std::string name_;
    const Weight weight_;
    const uint64_t cost_; // in cp
};

std::ostream& operator <<(std::ostream &out, const Equipment& equipment);

inline Weight Weight::operator +(const Weight& rhs) const {
    return { numerator*rhs.denominator + rhs.numerator*denominator, denominator*rhs.denominator };
}

inline Weight Weight::operator *(uint64_t rhs) const {
    return { numerator * rhs, denominator };
}

} // namespace fes

#endif // FES_EQUIPMENT_H_

